# `solidbase.info`

This is the source code for [`learn.solidbase.info`](https://learn.solidbase.info).

## Dependencies

- [Hugo](https://gohugo.io/getting-started/installing/)
- [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

## Run locally

```
git clone git@lab.allmende.io:solidbase/learn.git
cd learn
git submodule sync
git submodule update --init --recursive --remote
hugo serve
```

## Deployment

This repo uses [gitlab CI/CD](https://docs.gitlab.com/ce/ci/#exploring-gitlab-ci-cd) for deployment to [dokku](http://dokku.viewdocs.io/dokku/). See the [pipeline configuration](https://lab.allmende.io/solidbase/learn/blob/master/.gitlab-ci.yml) for details.  
The following Variables needs to be set under `Settings->CI/CD->Variables`:

- `APP_NAME_DEV`:  The SLD name for the staging instance, which will become accessible under `$APP_NAME_DEV.dokku.ecobytes.net`
- `APP_NAME`: A fqdn for the production instance
- `EMAIL`: email address for git
- `NAME`: name for git
- `SSH_PRIVATE_KEY`: A key for the dokku
- `SSH_SERVER_HOSTKEYS`: obtained by keyscanning the dokku, in our case by `ssh-keyscan dokku.ecobytes.net`

Deployment to production needs to be maually triggered in the pipeline view.

## Authors

- Carolin Gruber
- Jon Richter
- Johannes Winter

## License

CC0
