---
title: "Aktivität hinzufügen"
# date: 2019-04-27T13:49:38+02:00
weight: 14
pre: "<b>4.</b> "
---

Klicke auf „Aktivität hinzufügen“

![](/images/solidbase/de/Solidbase_exercises_4_1.png)

Der folgende Dialog erscheint:

![](/images/solidbase/de/Solidbase_exercises_4_2.png)

Gib der Aktivität einen Namen und ordne sie einer übergeordneten Aktivität zu. Z.B.:

- Name: Ackerbau,
- set associated activity to: “Solawi neue happy farm”

Klicke auf “Add”.

![](/images/solidbase/de/Solidbase_exercises_4_3.png)

Zunächst möchten wir dieser Aktivität auch Ausgaben hinzufügen.
