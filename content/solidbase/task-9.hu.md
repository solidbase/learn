---
title: "Edit expense category text"
# date: 2019-04-27T13:49:38+02:00
weight: 18
pre: "<b>9.</b> "
---

The purpose of expense categories are to structure the monetary expenses logically according to budget terms such as labor, administration, tools, insurance, etc.

One of the goals of the solidbase app is to help communicate the budget of your CSA to your members. With this goal in mind, the expense category texts have been implemented to allow to directly display a description for the expense categories when looking at the budget. For training SFS coordinators on budget building these texts can also be used for explaining whch expenses belong to which category.

The texts are displayed on the detailed view of one expense category. I.e: if you are on the overview level, where you see the pie chart in the centre and the list of all expense categories below:
![](/images/solidbase/en/Solidbase_exercises_9_1.jpg)

Then you can click on one row of the table to get to the respective detailed overview of the expense category.
(If you hover over the rows, you will get notified that it is possible to click on one row to focus. ![](/images/solidbase/en/Solidbase_exercises_9_2.jpg))

In the detailed overview, you see the pie chart on the left side and the text on the right side:
 ![](/images/solidbase/en/Solidbase_exercises_9_3.jpg)

There are example texts provided and a set of example expense categories that you can amend and extend to your needs.

{{% notice warning %}}
Currently: The example texts, once edited and categories deleted, can only be reset to the example texts as a set. I.e. as soon as you make modifications, the categories and texts are stored on your own account. To reload the example texts and categories, you would need to delete all your categories.
{{% /notice %}}

Now we are getting to the actual exercise! In order to edit the texts, click on "Configuration" in the upper left corner
![](/images/solidbase/en/Solidbase_exercises_9_4.jpg)

You might need to scroll down to see the expense category configuration:
![](/images/solidbase/en/Solidbase_exercises_9_5.jpg)

Now choose the cateogory "labor", choose your locale (your language) and then edit the text. Finally click *Save*.
You may find that in your language, the category texts are not displayed. The reason you can see in the expense category configuration. If for a certain locale the text is empty, then there is no text displayed.

## Changing the default texts
The default texts that are loaded for a new user or when all are deleted (as explained in the warning section) can be changed by users that have special priviledges. These can only be set by the author(s). Please contact [me](mailto:solidbase@solidarische-landwirtschaft.org) if you want to be granted *Super Admin Priviledges*.
