---
title: "Allocate expenses to months"
date: 2019-04-27T13:49:38+02:00
weight: 16
pre: "<b>6.</b> "
---

Click on, like in task 5, `add expesnses`

![](/images/solidbase/de/Solidbase_exercises_6_1.png)

Now click on `monthly view`

The dialog changes like:

![](/images/solidbase/de/Solidbase_exercises_6_2.png)

Now enter these dates:

- Select class: Bodenfruchtbarkeitsaufbau
- Name: Kalk
- September: 28
- Set associated activity to: Ackerbau

Klick on `save`

{{% notice tip %}}
You can see the changes when the monthly view is enabled
{{% /notice %}}

![](/images/solidbase/de/Solidbase_exercises_6_3.png)

Annual view:

![](/images/solidbase/de/Solidbase_exercises_6_4.png)

Monthly view:

![](/images/solidbase/de/Solidbase_exercises_6_5.png)
