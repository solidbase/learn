---
title: "Přidat činnost"
# date: 2019-04-27T13:49:38+02:00
weight: 14
pre: "<b>4.</b> "
---

Klikněte na `Přidat činnost`

![](/images/solidbase/cz/Solidbase_exercises_4_1.jpg)

Objeví se následující okno:

![](/images/solidbase/cz/Solidbase_exercises_4_2.jpg)

Pojmenujte činnost a přiraďte ji nadřazenou kategorii.

- Název: Ackerbau,
- nastavení nadřazené kategorie : “CSA”

Kliněte na “Přidat”.

Nyní se nová činnost objevína konci stránky:

![](/images/solidbase/cz/Solidbase_exercises_4_3.jpg)

klikněte na zelený odkaz abyste se dostali do této činnosti nižší úrovně:

![](/images/solidbase/cz/Solidbase_exercises_4_4.jpg)

Nyní můžeme přidávat náklady do této činnosti.
