---
title: "Budget laden"
date: 2019-04-27T13:49:38+02:00
weight: 12
pre: "<b>2.</b> "
---

Klicke auf „Lade“

![](/images/solidbase/de/Solidbase_exercises_2_1.png)

Es erscheint der folgende Dialog:

![](/images/solidbase/de/Solidbase_exercises_2_2.png)

Wähle ein Budget aus und klicke auf „Lade“