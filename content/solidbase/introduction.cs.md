---
title: "Úvod"
# date: 2019-04-27T13:48:18+02:00
weight: 1
---
Aplikace SolidBase je jednak plánovačem rozpočtu, tak i prostředím pro jeho prezentaci. Byla vytvořena jako <strong>vzdělávací nástroj</strong> pro zvyšování kapacit koordinátorů iniciativ sociální a solidární ekonomiky zejména v oblasti potravin.

Umožňuje snadno shromáždit a zobrazit roční <strong>náklady</strong> projektů a zobrazit je. Každý náklad lze zařadit do nákladové kategorie, která má vysvětlující texty. To umožňuje přesné umístně informací o nákladech, které jsou specifické pro agro-ekologické hospodaření, budování komunity a další náklady, které nejsou přímo spjaté s produkcí. Tyto informace mohou vést k lepšímu vhledu do nezbytných nákladů pro skutečně udržitelnou produkci, čímž se zvyšuje připravenost (potenciálních) členů přispívat adekvátní množství peněz.<br>Z výukového hlediska mohou tyto texty vzdělávat koordinátory systémů solidárních nákupních skupin o výdajových kategoriích, aby při tvorbě rozpočtu nezapoměli na žádné nákladové položky.

Systém využívá koncept předpřipravených <strong>činností</strong>, které usnadňují strukturování celého rozpočtu farmy do logických jednotek. Tyto činnosti mohou být například jednotlivé produkční kroky (polní produkce, zelinářství, pekárna), distribuční kanály (co je produkováno pro KPZ a co ne) nebo dokonce konkrétní produkční postupy (jeden žádek mrkve, měsíční náklady chovu krav).

Srovnání s předpřipravenými <strong>vzorovými rozpočty</strong> umožňuje - zejména začínajícím inciativám - nezapomenout při tvorbě rozpočtu na nezbytné nákladové položky.

