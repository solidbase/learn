---
title: "Přidat příjem"
# date: 2019-04-27T13:49:38+02:00
weight: 18
pre: "<b>8.</b> "
---

**Příjem** v aplikaci značí veškeré prostředky které vám přicházejí a nejsou spojeny s platbami od členů. Může se jednat o dotace či příjmy z prodeje mimo KPZ pokud nejsou zahrnuty v jiných *činnostech*. 

Klikněte na *Přidat příjem*

![](/images/solidbase/cz/Solidbase_exercises_8_1.jpg)

Objeví se následující dialog

![](/images/solidbase/cz/Solidbase_exercises_8_2.jpg)

VLože následující:

- Jméno: external funding
- Částka: 10000
- Zvolte činnost, do níž náklad spadá: „CSA”

Klikněte na *Přidat*.

Příjmy fungují podobně jako náklady. Lze je připojovat k činnostem prostřednictvím rolovacího menu "Zvolte činnost, do níž náklad/příjem spadá. Nicméně příjmy se nezobrazí v koláčovém grafu, který zobrazuje pouze náklady. Příjmy se zobrazují pouze na dvou místech. Jednak jsou zobrazeny v přehledu činností nižší úrovně, který shrnuje příjmy a náklady každé činnosti. 

![](/images/solidbase/cz/Solidbase_exercises_8_3.jpg)

A ještě podstatněji jsou zahrnuty do výpočtu podílu na jednoho člena:
![](/images/solidbase/cz/Solidbase_exercises_8_4.jpg)
