---
title: "Ausgabe hinzufügen in der Kategorie „Lohnkosten“"
date: 2019-04-27T13:49:38+02:00
weight: 15
pre: "<b>5.</b> "
---

Bleibe in der Aktivität „Ackerbau“. Wenn du in der übergeordneten Aktivität bist, dann gehe in die gerade angelegte Aktivität.

![](/images/solidbase/de/Solidbase_exercises_5_1.png)

Klicke auf „Ausgabe hinzufügen:

![](/images/solidbase/de/Solidbase_exercises_5_2.png)

Das folgende Fenster öffnet sich:

![](/images/solidbase/de/Solidbase_exercises_5_3.png)

Gib die folgenden Daten ein

- Select classe : Lohnkosten
- Name: Selbständiger B
- Menge: 35 000
- Set associated activity to: Ackerbau

Und klicke auf “Hinzufügen”

![](/images/solidbase/de/Solidbase_exercises_5_4.png)