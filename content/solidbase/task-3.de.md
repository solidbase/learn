---
title: "Neues (leeres) Budget erstellen"
date: 2019-04-27T13:49:38+02:00
weight: 13
pre: "<b>3.</b> "
---

Klicke auf

![](/images/solidbase/de/Solidbase_exercises_3_1.png)

am unteren Ende des Bildschirms.

![](/images/solidbase/de/Solidbase_exercises_3_2.png)

Klicke auf OK

Klicke auf „Edit activity“ am unteren Ende des Bildschirms

![](/images/solidbase/de/Solidbase_exercises_3_3.png)

Füge den Namen deiner Solawi ein und klicke auf „Speichern“:

- Name: Solawi neue happy farm

![](/images/solidbase/de/Solidbase_exercises_3_4.png)