---
title: "Einnahme hinzufügen"
date: 2019-04-27T13:49:38+02:00
weight: 18
pre: "<b>8.</b> "
---

Klicke auf „Einnahmen hinzufügen“

![](/images/solidbase/de/Solidbase_exercises_8_1.png)

Es erscheint der folgende Dialog:

![](/images/solidbase/de/Solidbase_exercises_8_2.png)

Fülle wie folgt aus: 

- Name: Subventionen
- Menge: 20000
- Set associated activity to : „Solawi neue happy farm”

Klicke auf “Hinzufügen”