---
title: "Einleitung"
date: 2019-04-27T13:48:18+02:00
weight: 1
---

Die solidbase Anwendung ist eine Umgebung zur Planung und Präsentation von Budgets. Es wurde als Bildungswerkzeug entwickelt, und dient dazu Buchhaltungskompetenzen aufzubauen innerhalb Initiativen der solidarischen Ökonomie, insbesondere im Bereich Lebensmittelproduktion.

Das Tool erlaubt die einfache Auflistung jährlicher Kosten eines Unternehmens und kann diese visuell darstellen. Es ist möglich Beschreibungen der Kostenpunkte hinzuzufügen, um den Mitgliedern solcher Initiativen transparent die Notwendigeit der Kosten zu vermitteln und Hintergrundinformationen zu geben.

Bereitgestellte Beispielbudgets vereinfachen die individuelle Erstellung des Budgets der einzelnen Initiativen, insbesondere in der Startphase. So wird sicher gestellt, dass alle relevanten Kostenpunkte betrachtet werden können.