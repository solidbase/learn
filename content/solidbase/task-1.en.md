---
title: "Logging in"
weight: 11
pre: "<b>1.</b> "
---

In order to use the app, you need to register with a solid account. You have to create your own account. Or you can test using the pre-prepared demo account (only for testing and familiarization with the tool).

To create a solid account, you need to choose a solid server. That's the place where the data of the app will be stored. The solid community provides several servers. Currently we advice to use the Solid Base development server on <a href="https://ld.solidbase.info" target="blank"> ld.solidbase.info</a>

Click on `https://ld.solidbase.info`

![](/images/solidbase/en/Solidbase_exercises_1_1.jpg)

Choose an username and password and click *Register*

![](/images/solidbase/en/Solidbase_exercises_1_2.jpg)


Once you created an account, you can log in to the app as follows.
Click on `https://app.solidbase.info/` and click on *Configuration*:

![](/images/solidbase/en/Solidbase_exercises_1_3.jpg)

Click *login* ![](/images/solidbase/en/Solidbase_exercises_1_4.jpg)

The following pop-up window opens:

![](/images/solidbase/en/Solidbase_exercises_1_5.jpg)

{{% notice warning %}}
Sometimes the pop-up hides behind other browser windows. Check your desktop if nothing happens when you click on the login button!
{{%/notice%}}


Please chose the solid server where you created an account. If you used the ld.solidbase.info server, click on the blue button with `ld.solidbase.info`. Otherwise, enter `https://ld.solidbase.info/` or the URL of another server in the field above and click *Go*

The following windows opens:

![](/images/solidbase/en/Solidbase_exercises_1_6.jpg)

Enter your username and password and click *Log In*. (It is possible to create account also here by clicking on *Create an account*. It does the same like *Register* button mentioned above.).

If you have chosen a server different to `ld.solidbase.info`, you will be asked to grant authorization to the solidbase app. Click *Authorize* in the following dialogue:

![](/images/solidbase/en/Solidbase_exercises_1_6a.jpg)

{{% notice warning %}}
Currently this doesn't authorize the app. You need to follow [this procedure](https://github.com/solid/userguide#using-third-party-apps) for `https://app.solidbase.info`  for authorization.
{{% /notice %}}
If you are logged in successfully it looks like this:

![](/images/solidbase/en/Solidbase_exercises_1_7.jpg)


Now you can click on *Chart* to start with your budgeting!
