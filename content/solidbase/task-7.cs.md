---
title: "Přesunout výdaj do jiné aktivity"
# date: 2019-04-27T13:49:38+02:00
weight: 17
pre: "<b>7.</b> "
---

Vytvořte jinou činnost v následujícími vlastnostmi:

- Název: Gärtnerei
- Činnost, s níž je spojena: CSA

Přehled činnosti CSA (činnost nejvyšší úrovně) se zobrazní následujícím způsobem: 

![](/images/solidbase/cz/Solidbase_exercises_7_1.jpg)

Nyní klikněte na činnost “Ackerbau“ a zobrazí se její podrobnosti ![](/images/solidbase/cz/Solidbase_exercises_7_2.jpg)

Klikněte na řádek nákladové kategorie, která obsahuje náklad, který chcete přesunout. Například `práce`.

Podrobné zobrazení bude vypadat následovně:

![](/images/solidbase/cz/Solidbase_exercises_7_3.jpg)

Klikněte na „Upravit náklad“

![](/images/solidbase/cz/Solidbase_exercises_7_4.jpg)

Nyní z rolovacího menu "Zvolte činnost, do níž náklad spadá" zvolte jinou činnost.

Kliněte na uložit. Nyní uvidíte, že náklady zmizel z aktivity "Ackerbau":

![](/images/solidbase/cz/Solidbase_exercises_7_5.jpg)

a byl přidán do aktivity "Gärtnerei"

![](/images/solidbase/cz/Solidbase_exercises_7_6.jpg)