---
title: "Introduction"
# date: 2019-04-27T13:48:18+02:00
weight: 1
---
The solidbase application is a budgeting planning and presentation environment developed as an <strong>educational tool</strong> for building accounting capacities for coordinators of initiatives out of the social and solidarity economy, first of all from the food sector.

It allows to easily collect and visualize the annual and monthly <strong>expenses</strong> of a members business. Each expense can be sorted into an expense category which is linked to an explanatory text. This allows the exact placement of information about expenses that are special to agro-ecological cultivation methods, community building and other not directly to production related costs. These information may lead to enhanced insight into the necessary costs for really sustainable production and by that increases the readiness of the (potential) member to contribute an adequate amount of money.<br>For training purposes these texts can also be used to educate SFS (Solidarity based Food System) coordinators on the expense categories not to forget any costs when creating a budget.

To facilitate the structuring of the whole farm into logical units the concept of nested <strong>activities</strong> is used. These can be business branches (farming, gardening, bakery), marketing channels (what is produced for CSA, what not?) or even production procedures (one bed of carrots, the monthly costs of a cow).

The comparison with predefined <strong>example budgets</strong> makes it more easy  esp. for starting initiatives to remember all necessary costs to build a budget for the own business.
