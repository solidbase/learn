---
title: "Add activity"
# date: 2019-04-27T13:49:38+02:00
weight: 14
pre: "<b>4.</b> "
---

Click on `Add activity`

![](/images/solidbase/en/Solidbase_exercises_4_1.jpg)

The following dialog appears:

![](/images/solidbase/en/Solidbase_exercises_4_2.jpg)

Name the activity and connect it to a superordinate activity.

- Name: Ackerbau,
- set associated activity to: “CSA”

Click on “Add”.

Now the new activity appears at the bottom of the page:

![](/images/solidbase/en/Solidbase_exercises_4_3.jpg)

click on the green link to go to this subactivity:

![](/images/solidbase/en/Solidbase_exercises_4_4.jpg)

Now we want to add expenses to this activity.
