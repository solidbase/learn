---
title: "Benutzung"
date: 2019-04-27T13:48:23+02:00
weight: 2
---

Solidbase speichert die Daten auf [SoLiD](https://solid.mit.edu/). Um die Anwendung, die App, nutzen zu können, musst du dich bei einem öffentlichen SoLiD server registrieren, wie zum Beispiel bei [solid.community](https://solid.community).

Da wir für die App neuste Technologien anwenden, ist es wichtig, dass du einen aktuellen Browser nutzt. Wir empfehlen [Firefox >= Quantum 65](https://www.mozilla.org/firefox/download/thanks/).
.
Nach der Registrierung, kannst du dich auf der **Config** - Seite einloggen. Klicke dazu auf den login Button und gebe deine Zugangsdaten ein.

Nach dem Einloggen, findest du die Funktionen der App auf der **Chart**-Seite.
