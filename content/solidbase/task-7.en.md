---
title: "Move expense to another activity"
# date: 2019-04-27T13:49:38+02:00
weight: 17
pre: "<b>7.</b> "
---

Create another activity with the following properties:

- Name: Gärtnerei
- Set associated activity to: CSA

The overview of the CSA activity (the highes activity) will show like this: 

![](/images/solidbase/en/Solidbase_exercises_7_1.jpg)

Now focus activity “Ackerbau“ by clicking on the green link "Ackerbau" ![](/images/solidbase/en/Solidbase_exercises_7_2.jpg)

Click on the row of the expense category that contains the expense we want to move. Like `labor`.

The detailed view looks like the following:

![](/images/solidbase/en/Solidbase_exercises_7_3.jpg)

Click on „Edit expense“

![](/images/solidbase/en/Solidbase_exercises_7_4.jpg)

Now select another activity from the dropdown list in „Set associated activity to“

Click save. Then you will see that the expense was deleted from the activity "Ackerbau":

![](/images/solidbase/en/Solidbase_exercises_7_5.jpg)

and added to the activity "Gärtnerei"

![](/images/solidbase/en/Solidbase_exercises_7_6.jpg)