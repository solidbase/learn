---
title: "Add expenses in category  `wages``"
# date: 2019-04-27T13:49:38+02:00
weight: 15
pre: "<b>5.</b> "
---

Select the activity on which you want to create this expenses

![](/images/solidbase/de/Solidbase_exercises_5_1.png)

Click on `add expenses`

![](/images/solidbase/de/Solidbase_exercises_5_2.png)

The following modal opens:

![](/images/solidbase/de/Solidbase_exercises_5_3.png)

Enter the following values:

- Select classe : Lohnkosten
- Name: Selbständiger B
- Amount: 35 000
- Set associated activity to: Ackerbau

And click on `Add`.

![](/images/solidbase/de/Solidbase_exercises_5_4.png)
