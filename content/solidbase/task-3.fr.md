---
title: "Create ne (empty) budget"
# date: 2019-04-27T13:49:38+02:00
weight: 13
pre: "<b>3.</b> "
---

Click on

![](/images/solidbase/de/Solidbase_exercises_3_1.png)

at the lower end of the screen.

![](/images/solidbase/de/Solidbase_exercises_3_2.png)

Click on OK

Click on `edit activity`  in the lower end of the screen

![](/images/solidbase/de/Solidbase_exercises_3_3.png)

Enter the name of your CSA and click on `Save`:

- Name: Solawi neue happy farm

![](/images/solidbase/de/Solidbase_exercises_3_4.png)
