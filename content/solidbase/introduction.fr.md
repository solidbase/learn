---
title: "Introduction"
# date: 2019-04-27T13:48:18+02:00
weight: 1
---

The solidbase application is a budgeting planning and presentation environment developed as an educational tool for building accounting capacities for coordinators of initiatives out of the social and solidarity economy, first of all from the food sector.

It allows to easily collect the annual expenses of a business and to visualize these. The expenses can be linked to explanatory texts, so that the membership can gain insight into the necessity of the height of the costs for the share of the produce.

The comparison with predefined example budgets make it more easy to remember all necessary costs and to build budgets for the own business esp. for starting initiatives.
