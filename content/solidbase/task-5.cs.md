---
title: "Přidání nákladů do činnosti"
# date: 2019-04-27T13:49:38+02:00
weight: 15
pre: "<b>5.</b> "
---

Klikněte na `Přidat výdaj`

![](/images/solidbase/cz/Solidbase_exercises_5_1.jpg)

Otevře se následující:

![](/images/solidbase/cz/Solidbase_exercises_5_2.jpg)

Zadejte tyto hodnoty:

- Zvolte kategorii: Práce
- Jméno: jméno zaměstnance
- Částka: roční náklady na zaměstnance
- Připojit k činnosti: Ackerbau

Poslední bod "připojit k činnosti" zvolí činnost, která má zahrnovat tento výdaj. Ve výchozím nastavení bude zvolena činnost v níž jste byli při kliknutí na "Přidat výdaj". 

A klikněte na `Přidat`.

![](/images/solidbase/cz/Solidbase_exercises_5_3.jpg)

Pokud najedete kurzorem na kategorii Práce, můžete na ni kliknout a získat podrobnější zobrazení. 

Klikněte na řádek a přejdete na následující podrobné zobrazení: 

![](/images/solidbase/cz/Solidbase_exercises_5_4.jpg)
V podrobném zobrazení můžete vidět nově vytvořené výdaje a případně je upravovat.

Kategorie jako je "Práce" jsou nastavovány na jiné úrovni správy a lze jim přidávat vysvětlující texty. Tyto texty se zobrazují v podrobnějším výpisu kategorií na pravé straně pod nadpisem "O výdajové kategorii"
Po kliknutí na tlačítko "Zpět na celkový rozpočet" se dostanete na přehled všech kategoií dané činnosti.

Navíc, je možné přepínat mezi ročním a měsíčním zobrazením výdajů použitím tlačítka přepnutí roční/měsíční náhled. 




