---
title: "Add income"
# date: 2019-04-27T13:49:38+02:00
weight: 18
pre: "<b>8.</b> "
---

Click on `Add income`

![](/images/solidbase/de/Solidbase_exercises_8_1.png)

The following dialog appears:

![](/images/solidbase/de/Solidbase_exercises_8_2.png)

Enter the following:

- Name: Subventionen
- Menge: 20000
- Set associated activity to : „Solawi neue happy farm”

Click on `Add`.
