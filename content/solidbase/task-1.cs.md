---
title: "Přihlášení"
weight: 11
pre: "<b>1.</b> "
---

Pro přihlášení do aplikace se je třeba přihlásit svým solid účtem. Musíte si vytvořit vlastní účet, či můžete využít připravené testovací prostředí (pouze pro seznámení s aplikací).

Pro vytvoření účtu solid si musíte zvolit solid server. Což je místo, kde jsou uložena data aplikace. Komunita Solid poskytuje několik serverů. Aktuálně doporučujeme využít vývojový server na  https://ld.solidbase.info:

Klikněte na https://ld.solidbase.info

![](/images/solidbase/cz/Solidbase_exercises_1_1.jpg)

Zvolte si uživatelské jméno a heslu a klikněte na "Register"

![](/images/solidbase/cz/Solidbase_exercises_1_2.jpg)


Jakmile je účet vytvořen, můžete se přihlásit do aplikace.
Přejděte na https://app.solidbase.info/ a klikněte na  Nastavení:

![](/images/solidbase/cz/Solidbase_exercises_1_3.jpg)

Kliněte na login ![](/images/solidbase/cz/Solidbase_exercises_1_4.jpg)

A objeví se toto vyskakovací okno:

![](/images/solidbase/cz/Solidbase_exercises_1_5.jpg)

Zvolte prosím solid server, kde jste vytvořili váš účet. Pokud jste použili při vytváření účtu server ld.solidbase.info server, klikněte na modré tlačítko s nápisem "ld.solidbase.info". Jinak vložte adresu https://ld.solidbase.info/ nebo adresu jiného serveru do pole nad tlačítkem a klikněte na "Go".

Objeví se toto okno:

![](/images/solidbase/cz/Solidbase_exercises_1_6.jpg)

Vložte vaše uživatelské jméno a heslo a klikněte na "Log In". Pro vytvoření nového účtu můžete i odsud kliknout na  *Create an account*.

Pokud jste zvolili jiný server než ld.solidbase.info, budete požádáni o autorizaci pro aplikaci. V následujícím okně klikněte na  "Authorize" :

![](/images/solidbase/cz/Solidbase_exercises_1_6a.jpg)

{{% notice warning %}}
Toto ovšem ještě autorizaci nezajistí, budete muset použít [tento postup](https://github.com/solid/userguide#using-third-party-apps) abyste autorizovali https://app.solidbase.info.
{{% /notice %}}
Pokud bylo přihlášení úspěšné uvidíte toto:

![](/images/solidbase/cz/Solidbase_exercises_1_7.jpg)


Nyní můžete kliknout na "Rozpočet" a začít!!
