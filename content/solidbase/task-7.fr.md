---
title: "Move expense to another activity"
# date: 2019-04-27T13:49:38+02:00
weight: 17
pre: "<b>7.</b> "
---

Create another activity with the following properties:

- Name: Gärtnerei
- Set associated activity to: Solawi happy farm

Now focus sctivity “Ackerbau“:

![](/images/solidbase/de/Solidbase_exercises_7_1.png)

Click on the row of the expense category that contains the expense we want to move. Like `wages`.

The detailed view looks like the following:

![](/images/solidbase/de/Solidbase_exercises_7_2.png)

Click on „Edit expense“

![](/images/solidbase/de/Solidbase_exercises_7_3.png)

Now select another activity from the dropdown list in „Set associated activity to“
