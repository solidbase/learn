---
title: "Ausgaben einem bestimmten Monat zuordnen"
date: 2019-04-27T13:49:38+02:00
weight: 16
pre: "<b>6.</b> "
---

Klicke dazu wieder, wie bei Aufgabe 5 auf „Ausgaben hinzufügen“:

![](/images/solidbase/de/Solidbase_exercises_6_1.png)

Klicke nun auf „monthly view“

Der Dialog ändert sich wie folgt:

![](/images/solidbase/de/Solidbase_exercises_6_2.png)

Gib nun die folgenden Daten ein:

- Select class: Bodenfruchtbarkeitsaufbau
- Name: Kalk
- September: 28
- Set associated activity to: Ackerbau

Klicke auf speichern.

{{% notice tip %}}
wenn du nun auf „Jahresansicht/Monatsansicht“ klickst, kannst du die beiden eben erstellten Ausgaben und ihre Unterschiede sehen:
{{% /notice %}}

![](/images/solidbase/de/Solidbase_exercises_6_3.png)

Jahressicht:

![](/images/solidbase/de/Solidbase_exercises_6_4.png)

Monatssicht:

![](/images/solidbase/de/Solidbase_exercises_6_5.png)