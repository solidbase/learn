---
title: "Allocate expenses to months"
date: 2019-04-27T13:49:38+02:00
weight: 16
pre: "<b>6.</b> "
---

Now we want to try to create an expense which is not evenly distributed over the year.

We do so by clicking on "Add expense" like in Exercise 5. 

The dialog opens:

![](/images/solidbase/en/Solidbase_exercises_6_1.jpg)

Now click on `monthly values`

The dialog changes like:

![](/images/solidbase/en/Solidbase_exercises_6_2.jpg)

Now enter these dates:

- Select category: soil fertility improvement
- Name: manure
- September: 390
- Set associated activity to: Ackerbau

Klick on `save`

Now click into the detailed view of the category "soil fertility improvement" by clicking on the respective row: 

![](/images/solidbase/en/Solidbase_exercises_6_3.jpg)


{{% notice tip %}}
You can see the changes when the monthly view is enabled
{{% /notice %}}

![](/images/solidbase/en/Solidbase_exercises_6_4.jpg)

