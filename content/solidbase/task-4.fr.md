---
title: "Add activity"
# date: 2019-04-27T13:49:38+02:00
weight: 14
pre: "<b>4.</b> "
---

Click on `Add activity`

![](/images/solidbase/de/Solidbase_exercises_4_1.png)

The following dialog appears:

![](/images/solidbase/de/Solidbase_exercises_4_2.png)

Name the activity and connect it to a superordinate activity.

- Name: Ackerbau,
- set associated activity to: “Solawi neue happy farm”

Click on “Add”.

![](/images/solidbase/de/Solidbase_exercises_4_3.png)


Now we want to add expenses to this activity.
