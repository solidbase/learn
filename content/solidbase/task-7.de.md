---
title: "Ausgabe verschieben zu einer anderen Aktivität"
date: 2019-04-27T13:49:38+02:00
weight: 17
pre: "<b>7.</b> "
---

Erstelle eine zweite Aktivität mit den folgenden Eigenschaften:

- Name: Gärtnerei
- Set associated activity to: Solawi happy farm

Nun gehe zur Aktivität “Ackerbau“:

![](/images/solidbase/de/Solidbase_exercises_7_1.png)

Klicke auf die Zeile der Ausgabenkategorie in der sich unsere zu verschiebende Ausgabe befinden. Zum Beipsiel „Lohnkosten“.

Die Detailansicht sieht wie folgt aus:

![](/images/solidbase/de/Solidbase_exercises_7_2.png)

Klicke auf „Edit expense“

![](/images/solidbase/de/Solidbase_exercises_7_3.png)

Nun wähle eine andere Aktivität aus der dropdown liste bei „Set associated activity to“