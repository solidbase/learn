---
title: "Rozdělení výdajů do jednotlivých měsíců"
date: 2019-04-27T13:49:38+02:00
weight: 16
pre: "<b>6.</b> "
---

Nyní chceme zkusti vytvořit výdaj, který není rovnoměrně rozdělen do celého roku.

Nejprve klikněte na "Přidat výdaj" jako ve cvičení 5. 

Otevře se dialogové okno:

![](/images/solidbase/cz/Solidbase_exercises_6_1.jpg)

Nyní klikněte na `měsíční hodnoty`

Dialogové okno se změní takto:

![](/images/solidbase/cz/Solidbase_exercises_6_2.jpg)

Nyní vložte následující údaje:

- Zvolit nákladovou kategorii: péče o půdu
- Název: hnůj
- září: 390
- Zvolit činnost do které výdaj spadá: Ackerbau

Klikněte na `uložit`

Nyní klikněte na řádek "Péče o půdu" pro zobrazení detailů této kategorie: 

![](/images/solidbase/cz/Solidbase_exercises_6_3.jpg)


{{% notice tip %}}
Změny uvidíte, pokud přepnete na měsíční zobrazení.
{{% /notice %}}

![](/images/solidbase/cz/Solidbase_exercises_6_4.jpg)

