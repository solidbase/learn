---
title: "Add expenses in category labor"
# date: 2019-04-27T13:49:38+02:00
weight: 15
pre: "<b>5.</b> "
---

Click on `add expenses`

![](/images/solidbase/en/Solidbase_exercises_5_1.jpg)

The following modal opens:

![](/images/solidbase/en/Solidbase_exercises_5_2.jpg)

Enter the following values:

- Select category : labor
- Name: freelancer B
- Amount: 35 000
- Set associated activity to: Ackerbau

The last point "set associated activity to" selects the activity on which you want to create this expense. The default setting when the dialog opens will be the activty you had focused when clicking the button. 

And click on `Add`.

![](/images/solidbase/en/Solidbase_exercises_5_3.jpg)

If you hover over the category labor, it is possible to click on it to get the more detailed view. 

Click on the row and you will go to the following detailed view: 

![](/images/solidbase/en/Solidbase_exercises_5_4.jpg)
On the detailed view you can see your newly created expense and edit it if required.

The categories like "labor" are defined on another level and it is possible to add an information text about them. These texts are displayed in the detailed view on the right side below the heading "About expense category ..."
Clicking on the "Overview" button leads back to the overview of all categories on this activity level.

Furthermore, it is possible to switch the view from annual to monthly by using the toggle "annual view".





