---
title: "Add income"
# date: 2019-04-27T13:49:38+02:00
weight: 18
pre: "<b>8.</b> "
---

**Income** means all money influx that is not related to regular payments by members. This can be subsidies, external funding or also other marketing channels if they are not mapped as different *activities*. 

Click on *Add income*

![](/images/solidbase/en/Solidbase_exercises_8_1.jpg)

The following dialog appears:

![](/images/solidbase/en/Solidbase_exercises_8_2.jpg)

Enter the following:

- Name: external funding
- Menge: 10000
- Set associated activity to : „CSA”

Click on *Add*.

Income works similar to expenses. It can be associated to an activity which you can select in  “Set associated activity to:”. It will not be shown in the pie chart however. The Pie chart only visualises costs. There are two places, where the income is considered.
First, it is shown in the overview of lower order activities which summarizes the income and the expenses of each activity :

![](/images/solidbase/en/Solidbase_exercises_8_3.jpg)

And most importantly, it is considered in the calculation of the share per member:
![](/images/solidbase/en/Solidbase_exercises_8_4.jpg)
