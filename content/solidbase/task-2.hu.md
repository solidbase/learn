---
title: "Loading a budget"
# date: 2019-04-27T13:49:38+02:00
weight: 12
pre: "<b>2.</b> "
---

Click on **load**

![](/images/solidbase/hu/Solidbase_exercises_2_1.jpg)

The following dialog appears:

![](/images/solidbase/hu/Solidbase_exercises_2_2.jpg)

Select a budget and click on `Load`.
