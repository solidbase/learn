---
title: "Usage"
date: 2019-04-27T13:48:23+02:00
weight: 2
---

Solidbase stores your data on <a href="https://solid.mit.edu/">SoLiD</a>. In order to use the app you need to register at a public SoLiD server, e.g. <a href="https://solid.community/">solid.community</a>. For the purpose of the training, we recommend an account on https://ld.solidbase.info. This is a server that has been set up in the framework of the SolidBase Project.

As this is cutting edge tooling, it is required to use an up-to-date browser. We recommend <a target=\"_blank\" href="https://www.mozilla.org/firefox/download/thanks/">firefox >= Quantum 65</a>.

After the registration, you can use your credentials to login, by pressing the login button on the <strong>Config</strong> page.
After logging in, you can use the app on the <strong>Chart</strong> page.
