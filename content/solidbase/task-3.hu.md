---
title: "Create new (empty) budget"
# date: 2019-04-27T13:49:38+02:00
weight: 13
pre: "<b>3.</b> "
---

Click on

![](/images/solidbase/hu/Solidbase_exercises_3_1.jpg)

at the lower end of the screen.

![](/images/solidbase/hu/Solidbase_exercises_3_2.jpg)

Click on *OK*,  the following pop-up appears:

![](/images/solidbase/hu/Solidbase_exercises_3_2a.jpg)

Click on *OK* if you want to save changes you've made recently.

Now a pop up windows appears that encourages you to enter the meta data of your budget:

![](/images/solidbase/hu/Solidbase_exercises_3_3.jpg)

You can set a **name**, choose the **year** and the **currency** and set the **number of members**. The budgeted amount will be devided by the number of members to get an idea of a possible individual contribution

You can also make your budget **private**. If not set to private, everyone can view your budget using the link that is printed at the bottom of the chart display (*Right-click -> copy to clipboard* to get a shareable version). If set to private it is copied to the private section of your Solid POD and only yu can access it. As this app is for outward communication of the budget the default mode is public. 

You can choose to use **decimals** for you amounts. If enabled, all numeric input boxes will allow to input two digits after the decimal point. The result of the calculation of the average share amount will be displayed with an precision of two decimal places anyway.

![](/images/solidbase/hu/Solidbase_exercises_3_4.jpg)

Click *Save*!

You can always change these settings by clicking "Edit budget"

![](/images/solidbase/hu/Solidbase_exercises_3_5.jpg)
