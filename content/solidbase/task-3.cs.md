---
title: "Vytvořit (prázdný) rozpočet"
# date: 2019-04-27T13:49:38+02:00
weight: 13
pre: "<b>3.</b> "
---

Klikněte na

![](/images/solidbase/cz/Solidbase_exercises_3_1.jpg)

v dolním rohu obrazovky.

![](/images/solidbase/cz/Solidbase_exercises_3_2.jpg)

Klikněte na *OK*,  objeví se následující vyskakovací okno:

![](/images/solidbase/cz/Solidbase_exercises_3_2a.jpg)

Klikněte na *OK*, chcete li uložit nedávné změny.

Nyní se objeví okno, které vás vyzývá vložit metadata vašeho rozpočtu:

![](/images/solidbase/cz/Solidbase_exercises_3_3.jpg)

Můžete nastavit **název**, zvolit **rok** a **měnu** a nastavit **počet členů**. Celková částka bude rozdělena počtem členů, abyste získali představu potřebného členského příspěvku. 

Váš rozpočet můžete také skrýt ostatním přepnutím tlačítka **private**. Pokud tak neučiníte mohou váš rozpočet vidět všichni uživatelé aplikace prostřednictvím odkazu, který je zobrazen na spodní části obrazovky rozpočtu (Pro sdílení *kliknte pravým tlačítme na odka za zvolte kopírovat odkaz*). Pokud je rozpočet soukromý máte k němu přístup pouze vy. Protože je tato aplikace určena k prezentaci rozpočtů je výchozí nastavení veřejné. . 

Pro vaše částky můžete používat **desetinná místa**. Pokud je aktivujete, všechny číselné hodnoty umožní vkládat až dvě číslovky za desetinou čárkou. Výsledkem výpočtu je průměrná cena podílu, která bude zobrazena s přesností na dvě desetinná místa vždy.

![](/images/solidbase/cz/Solidbase_exercises_3_4.jpg)

Klikněte *Uložit*!

Toto nastavení můžete vždy změnit kliknutím na "Upravit rozpočet"

![](/images/solidbase/cz/Solidbase_exercises_3_5.jpg)
