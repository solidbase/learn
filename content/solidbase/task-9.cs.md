---
title: "Upravit nákladové kategorie"
# date: 2019-04-27T13:49:38+02:00
weight: 18
pre: "<b>9.</b> "
---

Nákladové kategorie mají za cíl strukturovat peněžní výdaje podle jednoltivých oblastí jako je práce, administrace, nářadí, pojištění, atd.

Jedním ze záměrů aplikace solidbase je lepší komunikace rozpočetu členům vaší KPZ. S vědomím tohoto záměru byly připraveny vysvětlujíc texty, které umožňují přímé zobrazení popisu nákladové kategorie při prohlížení rozpočtu. Pro školení koordinátorů KPZ či jiných skupin tyto texty pomáhají také objasnit, jaké náklady spadají do jaké činnosti. 

Texty jsou zobrazeny při detailním zobrazení nákladové kategorie. Tedy, pokud máte zobrazen celkový rozpočet, kde vidíte koláčový graf ve středu a seznam nákladových kategorí pod ním:
![](/images/solidbase/cz/Solidbase_exercises_9_1.jpg)

Po té můžete kliknout na některou z nákladových kategorií a dostatnete se do podrobného přehledu kategorie. ![](/images/solidbase/cz/Solidbase_exercises_9_2.jpg))

V detailním zobrazení uvidíte koláčový graf na levé straně a popis kategorie vpravo:
 ![](/images/solidbase/cz/Solidbase_exercises_9_3.jpg)

V aplikaci jsou připraveny vzorové texty a nákladové kategorie, které můžete upravit či rozšířit dle vašich potřeb.

{{% notice warning %}}
Aktuální: Jakmile upravíte vzorové texty či smažete kategorie, je možné je vložit do rozpočtů pouze všechny najedou, tj. jakmile uděláte nějaké změny, kategorie a texty jsou uloženy ve vašem účtě. Abyste se vrátili k zobrazení vzorových textů a kategorií budete muset smazat všechny vámi vytvoření kategorie.
{{% /notice %}}

Pro úpravu textů klikněte na "Nastavení" v horní liště
![](/images/solidbase/cz/Solidbase_exercises_9_4.jpg)

Sjeďte dolů až tam, kde je nastavení nákladových kategorií:
![](/images/solidbase/cz/Solidbase_exercises_9_5.jpg)

Nyní můžete zvolit kategorii, např. "labor", zvolit váš jazyk a editovat text. Nakonec klikněte na  *Uložit*.


## Změna výchozích textů
Výchozí texty, které budou zobrazeny novým uživatelům, nebo texty které se zobrazí v případě že smažete všechny kategorie (viz sekce upozornění výše) lze měnit pouze uživateli se zvláštními právy. Tato práva mohou udělit pouze tvůrci aplikace. Pokud chcete *Práva Super Admina*, prosím kontaktujte [mne](mailto:solidbase@solidarische-landwirtschaft.org).
