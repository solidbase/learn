---
title: "Nahrát rozpočet"
# date: 2019-04-27T13:49:38+02:00
weight: 12
pre: "<b>2.</b> "
---

Klikněte na **Nahrát**

![](/images/solidbase/cz/Solidbase_exercises_2_1.jpg)

Objeví se následující okno:

![](/images/solidbase/cz/Solidbase_exercises_2_2.jpg)

Zvolte rozpočet a klikněte na `Nahrát`.
