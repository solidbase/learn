---
title: "Používání"
# date: 2019-04-27T13:48:23+02:00
weight: 2
---

Solidbase ukádá vaše data na <a href="https://solid.mit.edu/">SoLiD</a>. Pro použití aplikace je třeba se přihlásit na veřejný SoLiD server, např.: <a href="https://solid.community/">solid.community</a>.

Vzhledem k povaze této aplikace vyžadujeme aktualizovaný prohlížeč. Doporučujeme Firefox Quantum 65.

Po registraci můžete využít přihlašovací údaje k přihlášení na stránce Nastavení.

Po registraci můžete použít své údaje pro přihlášení prostřednictvím tlačítka <strong>Nastavení</strong> v horní liště. Po úspěšném přihlášení můžete používat aplikaci na stránce <strong>Rozpočet</strong>.
