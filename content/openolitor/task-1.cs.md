---
title: "Přidat člena"
date: 2019-04-27T02:35:11+02:00
weight: 11
pre: "<b>1.</b> "
---

Přejděte na přehled všech členů prostřednictvím položky menu Člen - > Členové

![](/images/openolitor/task1_cz_1.png)

Stiskněte tlačítko *vytvořit podílníka*.

![](/images/openolitor/task1_cz_2.png)

Otevře se podrobné zobrazení.
Všechna pole s červeným okrajem jsou povinná. Tlačítko "Přidat člena" zůstává neaktivní dokud nebou vyplněna všechna povinná pole.

{{% notice tip %}}
Vyplňte nejprve podrobnosti kontaktu a členské podrobnosti (adresa) předtím než stisknete "Přidat člena". To vám umožní kopírovat informace (adresu a jméno) do položky "Platební detaily".
{{% /notice %}}

![](/images/openolitor/task1_cz_3.png)

Po prvním uložení se objeví další detaily. Nová pole jsou: "Úkoly", "Podíly", "Faktury". Nyní přidáme členovi podíl v sekci "Podíly" a to následovně:

{{% notice note %}}
Člen může disponovat více než jednou kontaktní osobou, například rodiny či přátelé, kteří se o podíl dělí. V systému jsou zavedeni jako jeden člen, ale mohou mít více kontaktních osob.
{{% /notice %}}

![](/images/openolitor/task1_cz_4.png)

K přidání podílu členovi stiskněte "nový podíl".

![](/images/openolitor/task1_cz_5.png)

Na pravé straně obrazovky najdete průvodce tímto procesem. Nejprve zvolte druh podílu, po té distribuci (např. den) a následně místo (např. do kterého výdejního místa) a nakonec počáteční datum.


{{% notice note %}}
Pokud chcete zahájit dodávky v počáteční datum, prosím zvolte jako počáteční den předem.  

{{% /notice %}}

Klikněte na "vytvořit podíl" a podíl se objeví v přehledu členského profilu:

![](/images/openolitor/task1_cz_6.png)

Pokud kliknete na číslo pdílu (označené žlutě na obrázku níže), objeví se detaily podílu a umožní se úpravy jako mazání, úpravy konta či druhu distribuce případně manuálního vytvoření faktury.

![](/images/openolitor/task1_cz_7.png)
