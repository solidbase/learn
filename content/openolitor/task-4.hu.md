---
title: "Add new person category"
date: 2019-04-27T02:35:11+02:00
weight: 14
pre: "<b>4.</b> "
---

Go to menue item Setting -> Person categories: 

![](/images/openolitor/task4_hu_1.jpg)

Click "Create person category"

![](/images/openolitor/task4_hu_2.jpg)

![](/images/openolitor/task4_hu_3.jpg)

Fill in the details as follows:

- Person category: AG- digiTrans
- Description: working group for digital transformation

![](/images/openolitor/task4_hu_4.jpg)

Click save

![](/images/openolitor/task4_hu_5.jpg)
