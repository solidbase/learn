---
title: "Lieferplanung erstellen"
date: 2019-04-27T02:35:11+02:00
weight: 15
pre: "<b>5.</b> "
---

Go to menu item Deliveries -> Planning

![](/images/openolitor/task5_hu_1.jpg)

The overview shows all planned deliveries and their current status. To generate a new delivery for the next Week click on the button  „+ Neue Lieferplanung generieren“

![](/images/openolitor/task5_hu_2.jpg)

The following vie appears: 

![](/images/openolitor/task5_hu_3.jpg)

All subscription types that have been set to "Is planned regularly" will appear directly. It is possible to add other subscription types to the planning or delete ones you don't intend to plan.
Now you can add products from the product list on by drag and drop to the respective subscription types. For each subscription type and each distribution a separate box appears and can be filled with products. If there are more than one producer for a product you have to select one. The row in the box is marked in red as it is a mandatory field:

![](/images/openolitor/task5_hu_4.jpg)

When all boxes are planned, you can click on "Planning Save" 

![](/images/openolitor/task5_hu_5.jpg)

and then click "Delivery planning complete"

![](/images/openolitor/task5_hu_6.jpg)

After completing the delivery planning, it is still possible to edit the content of the planned boxes. However, it is not possible to change the number of members for that planned date. I.e. for CSAs where cancellation is allowed, it would not be possible to considered further cancellation in the planning process afte the delivere planning was completed. 

Now the following options appear in the menu:


![](/images/openolitor/task5_hu_7.jpg)

If you click "Settle delivery planning", the whole planning is closed and no modification is possible.

If you click "Show depot deliveries", the program will switch to the menu item Deliveries -> Depot deliveries and selects the depot information for the just created delivery planning. Choose this option now: 

![](/images/openolitor/task5_hu_8.jpg)

Now select the rows by ticking the checkbox and select "Print delivery slipt"

![](/images/openolitor/task5_hu_9.jpg)

The following window appears on the right side of the display:

![](/images/openolitor/task5_hu_10.jpg)

Click "Create".

If more than on depot was selected, in stead of one pdf file, a zip file including multiple pdfs is generated and downloaded. 

There is also the option to select individual templates or download the information as a JSON file by chosing "Date extract" from the Template drop down list. 

![](/images/openolitor/task5_hu_11.jpg)

The delivery slips and overvew of box contents includes required information for depots, members and suppliers. 