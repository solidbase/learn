---
title: "Přidat nový druh podílu"
date: 2019-04-27T02:35:11+02:00
weight: 12
pre: "<b>2.</b> "
---

Přejděte na Nastavení - > Druhy podílu

![](/images/openolitor/task2_cz_1.png)

Klikněte na "Vytvořit typ podílu" a objeví se nové okno:

![](/images/openolitor/task2_cz_2.png)

Vyplňte pole s červeným okrajem takto:

- Název: zvolte název typu
- Četnost závozů: týdně
- Cena: zvolte cenu

{{% notice note %}}
OpenOlitor aktuálně umožňuje nastavit cenu pouze pro závoz. Ne cenu za měsíc, jak bývá časté. Nicméně, je možné vložit měsíční cenu a generovat měsíční faktury s využitím též logiky. To znamená, i když vložíte cenu za závoz a vytvoříte fakturu za X závozů, můžete nahradit slovo "závoz" slovem "měsíc" bez jakéhokoliv dopadu.
Pouze pokud používáte automatickou připomínku platby to hraje roli. Předpokládáme-li, že závozy = měsíce, znamená že platíme 1 závoz/měsíc, ale dostaneme 4-5 podílů. OpenOlitor stojí na systému bilancí. Platba a doklad za 1 závoz zvýší vaši bilanci o 1. Závoz na jeden týden sníží bilanci o 1. Takže, když nastavíme, že závoz=měsíc na finančí straně tak se vše bilance bude soustavně snižovat. Dokud nezavedeme pojem "cena za měsíc" existuje jednoduché, ale neintuitivní řešení.
1. ignorujte bilance
2. uvažujete o ceně za závoz jako ceně za měsíc
3. nastavte "minimální stav bilance" na výrazně negativní číslo například -1000.

{{% /notice %}}

- Období dodávek: 12 měsíců
- Období upozornění: 4 týdny
- Minimální stav bilance: -1000 (viz výše)
- Pravidelnost: ano

Klikněte na "vytvořit druh podílu"

Po uložení se objeví nová sekce: "

![](/images/openolitor/task2_cz_3.png)

Nyní musíme vygenerovat novou "Distribuci". To znamená, definovat jak bude druh podílu zavážen. V sekci plánování závozů je naplánován týdenní závoz pro každou Distribuci.

Klikněte na plus v pravém horním rohu: ![](/images/openolitor/task2_en_4.jpg) a a objeví se nový řádek v sekci "Distribuce:

![](/images/openolitor/task2_cz_5.png)

Vyplňte pole:

- Označení:  zvolte jakékoliv jméno
- Den závozu: zvolte den
- Odběrné místo: Přidělte jedno nebo více odběrných míst, kde mohou členové v daný den vyzvedávat podíly.

Klikněte na "Uložit"

![](/images/openolitor/task2_en_6.jpg)

Další krok je velmi důležitý a trochu skrytý. Pro tuto nově vytvořenou distribuci potřebujeme vytvořit "Data závozů". To znamená definovat data, kdy bude tento druh podílu zavážen. Pouze když jsou data nastavena objeví se typ podílu na obrazovce plánování závozů!

Pro vytvoření datmů, klikněte na Kód distribuce:

![](/images/openolitor/task2_cz_7.png)

Na pravé straně se objeví vyskakovací okno.

![](/images/openolitor/task2_cz_8.png)

Můžete buď zadat data ručně kliknutím na +. Nebo můžete generovat data automaticky, podle dříve definovaných četností a zvoleného data distribuce. Pro tento krok kliněte na kalkulačku:

![](/images/openolitor/task2_cz_9.png)

Zvolte dobu trvání od kdy se mají data generovat a klikněte na "generovat data závozů".  

![](/images/openolitor/task2_cz_10.png)

V závislosti na tom jaké jste zvolili četnosti závozů se budou data generovat.

![](/images/openolitor/task2_cz_11.png)

Data je také potom monžné přidávat ručně.
