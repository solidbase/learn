---
title: "Create work opportunity"
date: 2019-04-27T02:35:11+02:00
weight: 18
pre: "<b>8.</b> "
---

Go to the menu item Participation -> work opportunities

![](/images/openolitor/task8_hu_1.jpg)

Click on "Create work opportunity". The following detailed view opens: 

![](/images/openolitor/task8_hu_2.jpg)

Fill in the information as follows: 

- Title: Weeding
- Citiy: Witzenhausen
- Start: 01.06.2019, 8:00
- End: 01.06.2019, 12:00

Click "Create work opportunity".

A new option appears in the basic data under "Actions". It is possible to multiply the event if this work opportunity is recurring. 

![](/images/openolitor/task8_hu_3.jpg)

Click on "+Multiply" and the following view opens:

![](/images/openolitor/task8_hu_4.jpg)

Choose a weekly frequency and an end date and click "Create work opportunity".

Go back to the overview of work opportunities:

![](/images/openolitor/task8_hu_5.jpg)

Die Angebote sind erst dann im Mitgliederportal zu sehen, wenn sie freigeschaltet wurden. Klicke dazu auf ein Arbeitsangebot hinein und wähle „Freischalten“.

![](/images/openolitor/task8_hu_6.jpg)
