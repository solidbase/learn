---
title: "OpenOlitor"
date: 2019-04-27T02:15:59+02:00
weight: 1
---

{{% notice tip %}}
[Allgemeine Bedienhinweise](./general-handling)
{{% /notice %}}


Beispielhafte Aufgaben:

1. [Neues Mitglied hinzufügen](./task-1)
2. [Neuen Anteilstyp erstellen](./task-2)
3. [Neues Produkt anlegen](./task-3)
4. [Neue Personenkategorie anlegen](./task-4)
5. [Lieferplanung erstellen](./task-5)
6. [Monatliche Abrechnung erstellen und SEPA XML generieren](./task-6)
7. [Email an alle Mitglieder einer Abholstelle schreiben](./task-7)
8. [Arbeitsangebot erstellen](./task-8)