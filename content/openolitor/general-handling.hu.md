---
title: "General handling"
date: 2019-04-27T02:31:56+02:00
weight: 5
---

![](/images/openolitor/generalhandling_hu_1.jpg)
red boarder -> has to be filled our mandatory field

![](/images/openolitor/generalhandling_hu_2.jpg)

![](/images/openolitor/generalhandling_hu_3.jpg)

Button with a selection of functions: Click on one drop down option and the action will be started like clicking on a button.