---
title: "Obecné instrukce"
date: 2019-04-27T02:31:56+02:00
weight: 5
---

![](/images/openolitor/generalhandling_en_1.jpg)
červený okraj -> povinné pole, musí se vyplnit

![](/images/openolitor/generalhandling_en_2.jpg)

![](/images/openolitor/generalhandling_en_3.jpg)

Tlačítko s výběrem funkcí: klikněte na možnost z rolovacího tlačítka ke spuštění akce.
