---
title: "Nastavení plánu závozů"
date: 2019-04-27T02:35:11+02:00
weight: 15
pre: "<b>5.</b> "
---

Přejděte na Závozy -> Plánování

![](/images/openolitor/task5_cz_1.jpg)

Přehled zobrazuje všechny plánované závozy a jejich aktuální stav. K vytvoření nového závozu na další týden klikněte na "+ generoavt nový plán závozů"

![](/images/openolitor/task5_cz_2.jpg)

Objeví se následující okno: 

![](/images/openolitor/task5_cz_3.jpg)

Všechny druhy podílů, kter émají nataveno "jsou plánovány pravidelně" se objeví rovnou. Je možné přidat další druhy podílů do plánování nebo smazat ty které nechcete plánovat. 
Nyní můžete přidat produkty se seznamu produktů přetažením do daného druhu podílu. Pro každý druh podílu a každou distribuci se objeví samostatný boxík, který je možné naplnit produkty. Pokud jeden produkt dodává více dodavatelů musíte zvolit jen jednoho. Červně označená pole jsou povinná. :

![](/images/openolitor/task5_cz_4.jpg)

Jakmile jsou všechny boxíky naplánovány klikněte na "Plánovní uložit" 

![](/images/openolitor/task5_cz_5.jpg)

a potom na "Plánování závozů dokončeno"

![](/images/openolitor/task5_cz_6.jpg)

Po dokončení plánování závozů je stále možné upravovat obsah plánovaných podílů. Nicméně není možné již měnit počet členů na plánvoané datum. Tedy pro KPZ, kde je možné rušit odběry, nebude možné rušit podíly po té co bude proces plánování dokončen. 

Nyní se objeví následující menu:


![](/images/openolitor/task5_cz_7.jpg)

Pokud kliknete na "Zaúčtovat plán závozů" celo plánování bude dokončeno a nebudou možné úpravy. 

Pokud kliknete na "Zobrazit výdejní místa" program se přepne do menu Dodávky -> Výdejní místa a vybere informaci o výdejním místě pro právě naplánované závozy. Nyní zvolte tuto možnost: 

![](/images/openolitor/task5_cz_8.jpg)

Zvolte řádek kliknutím na zaškrtávací políčko a klikněte na "Tisk listu závozu" 

![](/images/openolitor/task5_cz_9.jpg)

Na pravé straně obrazovky se objeví toto okno.

![](/images/openolitor/task5_cz_10.jpg)

Klikněte na "vytvořit".

Pokud bylo zvoleno více než jedno výdejní místo tak na místo jednoho pdf se stáhne zip soubor s více pdf soubory. 

Je možné zvolit také jednotlivé šablony či stáhnout informaci jako JSON file zvolením možnosti "Výtah ze souborů" v rolovacím menu.
![](/images/openolitor/task5_cz_11.jpg)

The delivery slips and overvew of box contents includes required information for depots, members and suppliers. 