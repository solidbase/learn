---
title: "Add new member"
date: 2019-04-27T02:35:11+02:00
weight: 11
pre: "<b>1.</b> "
---

Go to the overview of all participants/members via the menue item Member - > Members

![](/images/openolitor/task1_en_1.jpg)

Push the button "Create member"

![](/images/openolitor/task1_en_2.jpg)

A detailed view opens.
All fields with a red border are mandatory fields. The button "Create member" remains inactive until all mandatory fields have been filled out.

{{% notice tip %}}
Fill out the details of the contact details and the member details (address) first before pushing the button "Create member". This allows to copy information (address and name) over to the tap "Payment details".
{{% /notice %}}

![](/images/openolitor/task1_en_3.jpg)

New fields will appear on the member detail overview after the member was saved the first time. The new fields are: "Tasks", "Subscriptions", "Invoices". We are going to add a Subscription share to the member in the section "Subscriptions" as follows:

{{% notice note %}}
A member can have more than one contact person. For example families or friends, who share a subscription. They act as one member with one address but can have multiple contact persons.
{{% /notice %}}

![](/images/openolitor/task1_en_4.jpg)

To add a subscription share to the member push "new subscription".

![](/images/openolitor/task1_en_5.jpg)

On the right side of your display you will be guided through a selection process. First chose the subscription type, then the distribution (e.g. the day) and then the type of distribution (e.g. which depot) and finally a start date. 


{{% notice note %}}
If you want to start the delivery on the day of the start day that you just selected, please select one day in advance.

{{% /notice %}}

Click on "create subscription" and the subscription will appear in the overview of the member:

![](/images/openolitor/task1_en_6.jpg)

If you click on the subscription number (marked in yellow on the picture below), the subscription details will open and allows for edits such as deleting, modify the balance, modify the tpye of distribution, create an invoice manually.

![](/images/openolitor/task1_en_7.jpg)
