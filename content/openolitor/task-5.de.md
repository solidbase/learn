---
title: "Lieferplanung erstellen"
date: 2019-04-27T02:35:11+02:00
weight: 15
pre: "<b>5.</b> "
---

Gehe auf den Menüpunkt Lieferungen -> Planung:

![](/images/openolitor/Aufgabe5_1.jpg)

Hier werden die geplanten Lieferungen angezeigt und deren Status. Um einen neue Lieferplanung für die bevorstehende Woche zu generieren

Klicke auf den Button „+ Neue Lieferplanung generieren“

![](/images/openolitor/Aufgabe5_2.jpg)

Die folgenden Ansicht erscheint:

![](/images/openolitor/Aufgabe5_3.jpg)

Alle Anteilstypen die als „wird standardmäßig geplant“ eingestellt sind erscheinen. Es ist möglich die Anteilstypen aus der Planung zu löschen oder wieder hinzuzufügen. Jetzt können die Produkte aus der Liste der Produkt-Angebote per „drag and drop“ zu den jeweiligen Anteilstypen hineingezogen werden. Pro Anteilstyp, pro Vertrieb kann geplant werden. Wenn mehrere ProduzentInnen das Gemüse produzieren, muss eine Auswahl stattfinden. Die betreffende Zeile im Korb ist rot markiert:

![](/images/openolitor/Aufgabe5_4.jpg)

Wenn alle Anteilstypen geplant sind, kannst du auf „Planung speichern“

![](/images/openolitor/Aufgabe5_5.jpg)

und dann „Lieferplanung abschließen“

![](/images/openolitor/Aufgabe5_6.jpg)

klicken.

Nachdem die Lieferplanung abgeschlossen wurde, ist es noch möglich die Planung zu editieren, also Inhalte der Anteilstypen zu verändern. Was nicht mehr möglich ist, ist es die Anzahl der auszuliefernden Anteile zu ändern. In Solawis, in denen es möglich ist auszusetzen, wäre es also nicht mehr möglich das Anteil für die Woche zu stornieren für die die Lieferplanung bereits abgeschlossen wurde.

Jetzt erscheinen im Menü die folgenden Optionen:

![](/images/openolitor/Aufgabe5_7.jpg)

Unter „Lieferplanung verrechnen“ ist die gesamte Planung beendet und keine Modifikation ist mehr möglich.

Unter „Lieferungen an Abholstationen anzeigen“ springt das Programm zum Menüpunkt Lieferungen -> Lieferungen an Abholstationen und zeigt die Anteile pro Abholstation an, die zu der gerade erstellen Lieferplanung gehört. Wähle diese Option:

![](/images/openolitor/Aufgabe5_8.jpg)

Wähle jetzt die beiden Zeilen aus, indem du auf die Checkbox klickst und wähle „Lieferschein drucken“

![](/images/openolitor/Aufgabe5_9.jpg)

Das folgende Fenster erscheint auf der rechten Seite des Bildschirms:

![](/images/openolitor/Aufgabe5_10.jpg)

Klicke auf „Erstellen“.

Da mehrerer Abholstellen ausgewählt sind, wird ein zip File herunter geladen.

Es gibt auch die Möglichkeit andere Vorlagen auszuwählen, oder den Datenextrakt als JSON file herunter zu laden:

![](/images/openolitor/Aufgabe5_11.jpg)

Die Lieferscheine und Korbübersichten beinhalten die Informationen für Abholstellen, Mitglieder und Lieferanten.
