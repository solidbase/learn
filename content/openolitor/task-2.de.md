---
title: "Neuen Anteilstyp erstellen"
date: 2019-04-27T02:35:11+02:00
weight: 12
pre: "<b>2.</b> "
---

Gehe auf den Menüpunkt Einstellungen -> Anteilstypen

![](/images/openolitor/Aufgabe2_1.jpg)

Klicke auf „+ Anteilstyp erstellen“ und es öffnet sich das folgenden Fenster:

![](/images/openolitor/Aufgabe2_2.jpg)

Fülle die Roten Felder wie folgt aus:

- Name: den Namen für den Anteilstyp
- Lieferrhythmus: wöchentlich
- Preis: entspricht dem Richtwert.

{{% notice note %}}
Laut OpenOlitor können wir derzeit nur einen Preis pro Lieferung eintragen. Es ist jedoch möglich, den monatlichen Preis hier einzutragen, und offenen Posten zu generieren die eine Lieferung mit einem Monat gleichsetzen. Dass statt 1 Lieferung 4-5 Lieferungen pro Monat stattfinden können ist erst dann wichtig, wenn Mahnungen aus dem System heraus generiert werden sollen. Es gibt das Konzept des Guthabens. Das Guthaben wird in Lieferungen gezählt. Wenn also eine Rechnung von 1 Lieferung eigentlich 4-5 Lieferungen entspricht, so haben wir ein Guthaben von -3 Lieferungen. Langfristig soll es in der Dropdown liste die Möglichkeit „monatlich“ zu wählen. Da das „Guthaben“ Konzept in Deutschland eigentlich nicht relevant ist, können wir „pro Lieferung“ „pro Monat“ gleichsetzen. Wir müssen dafür nur das Mindestguthaben auf eine hohe negative Zahl, z.B. -1000 setzen.
{{% /notice %}}

- Laufzeit: 12 Monate
- Kündigungsfrist: 4 Wochen
- Guthaben Mindestbestand: -1000 (siehe Kommentar oben)
- Wird standardmäßig geplant: Ja

Drücke auf „Anteilstyp speichern“

Dann erscheint ein neuer Abschnitt ganz unten: „Vertrieb“

![](/images/openolitor/Aufgabe2_3.jpg)

Wir müssen jetzt einen neuen „Vertrieb“ generieren. Also wie wird der Anteilstyp ausgeliefert. Pro Vertrieb ist es möglich einen Korb zu planen.

Drücke auf das Plus-Zeichen rechts in der Ecke: ![](/images/openolitor/Aufgabe2_4.jpg) und es erscheint eine neue Zeile im Abschnitt „Vertrieb“:

![](/images/openolitor/Aufgabe2_5.jpg)

Fülle wie folgt aus:

- Bezeichnung: (wie du möchtest)
- Liefertag: Suche dir einen Tag aus
- Abholstationen: Ordne dem Tag ein oder mehrere Abholstationen hinzu

Drücke auf „Speichern“

![](/images/openolitor/Aufgabe2_6.jpg)

Ganz wichtig ist es jetzt noch „Lieferdaten“ einzugeben. Also an welchen Daten genau, dieser Anteil verteilt wird. Die Funktion ist sehr wichtig, aber etwas versteckt.

Klicke dazu auf die Vertriebsnummer:

![](/images/openolitor/Aufgabe2_7.jpg)

Auf der rechten Seite öffnet sich ein Fenster.

![](/images/openolitor/Aufgabe2_8.jpg)

Hier kannst du entweder Manuell durch klicken auf das + einzelne Daten generieren. Oder durch klick auf den Taschenrechner eine Liste von Daten generieren.

Klick auf den Taschenrechner:

![](/images/openolitor/Aufgabe2_9.jpg)

Wähle den Zeitraum aus von wann bis wann Lieferdaten generiert werden sollen. Und drücke auf „Lieferdaten erstellen“.

![](/images/openolitor/Aufgabe2_10.jpg)

Je nachdem wie du den Lieferhythmus eingestellt hast werden hier wöchentliche, zweiwöchentliche oder monatliche Daten generiert.

![](/images/openolitor/Aufgabe2_11.jpg)

Du kannst nachträglich Daten löschen oder durch das + wieder manuell hinzufügen.
