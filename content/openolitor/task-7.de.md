---
title: "Email an alle Mitglieder einer Abholstelle schreiben"
date: 2019-04-27T02:35:11+02:00
weight: 17
pre: "<b>7.</b> "
---

Gehe zu Einstellungen -> Abholstationen

![](/images/openolitor/Aufgabe7_1.jpg)

Wähle Abholstelle 2 aus indem du die checkbox anklickst. Wähle jetzt aus dem Menü button „Email an Kunden vershicken“

![](/images/openolitor/Aufgabe7_2.jpg)

Ein externe Fenster öffnet sich von deinem Mail client, mit den relevanten E-Mail Adressen der Mitglieder von Abholstelle 2

{{% notice note %}}
Es gibt auch die Möglichkeit, direkt über OpenOlitor Emails zu verwenden, ohne die Hilfe eines externen Email programms (e.g. Thunderbird, Outlook, etc.). Diese findet ihr unter dem Menüpunkt „E-Mail Formular“.
{{% /notice %}}