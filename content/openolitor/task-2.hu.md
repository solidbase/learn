---
title: "Add new subscription type"
date: 2019-04-27T02:35:11+02:00
weight: 12
pre: "<b>2.</b> "
---

Go to menue item Settings - > subscription type 

![](/images/openolitor/task2_hu_1.jpg)

Click on "Create a subscription type". A new view opens: 

![](/images/openolitor/task2_hu_2.jpg)

Fill in the fields with red boardes as follows: F

- Name: choose a name for the subscription tpye
- delivery frequency: Weekly
- Price: Select a reference price

{{% notice note %}}
OpenOlitor currently only allows to set a price per delivery. Not a price per month as common in some initiatives. However, it is possilbe to enter the monthly price and generate monthly invoices by using the same logic. This means, even if if says you enter price per deliver and create invoices for x deliveries, we can replace "deliveries" by "months" without any impact. 
Only when we use the automatic payment remindes option this plays a role. Assuming deliveries = months means that we pay 1 delivery/months but get 4-5 actual boxes delivered. OpenOlitor supports the concept of balances. Paying and invoice for 1 delivery increases your balance by 1. Getting one week delivered decreases your balance by 1. So if we set delivery=months on the financial side, your balance would constantly decrease. Until we can implement the concept of "price per month", there is a simple yet unintuitive workaround.
1. ignore the concept of balance
2. think price per delivery = price per months
3. set "credit balance minimum" to a large negative number, for example -1000. 

{{% /notice %}}

- Contract period: 12 months
- Notice periode: 4 weeks
- Credit balance minimum: -1000 (see comment above)
- Is planned regularly: Yes

Click on "create subscription type"

After saving a new section appears: "

![](/images/openolitor/task2_hu_3.jpg)

Now we need to generate a new "Distribution". This means to define how the subscription type can be delivered. In the delivery planning section, a weekly delivery is planned for each Distribution. 

Click on the plus sign in the right upper corner: ![](/images/openolitor/task2_hu_4.jpg) and a new row appears in the section "Distribution:

![](/images/openolitor/task2_hu_5.jpg)

Fill the fields as follows: 

- Designation:  give a name you like
- Delivery day: Chose a day
- Depots: Allocate one or more depots from where your members can collect their goods on that day. 

Click on "Save"

![](/images/openolitor/task2_hu_6.jpg)

The next step is very important and a bit hidden. For this newly generate Distribution, we need to create "Delivery dates". This means, define the dates, on which this subscription type will be delivered. Only if delivery dates have been set, the subscription and distribution will appear on the delivery planning dashboard!

To generate those dates, click on the Distribution Id:

![](/images/openolitor/task2_hu_7.jpg)

On the right side a window pops up.

![](/images/openolitor/task2_hu_8.jpg)

You have the choice to either manually add dates by clickint on the + sign. Or you can generate dates automatically according to the previously definded delivery frequency and the chosen distribution date. To do so, click on the calculator. 

Click on the calculator: 

![](/images/openolitor/task2_hu_9.jpg)

Chose a duration from when until when the dates should be generated and click "generate delivery dates". 

![](/images/openolitor/task2_hu_10.jpg)

Depending on how you selected your delivery frequency, weekly, biweekly or monthly dates will be generated. 

![](/images/openolitor/task2_hu_11.jpg)

It is also possible to add or delete dates manually after generating them automatically.
