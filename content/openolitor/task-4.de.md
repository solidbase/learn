---
title: "Neue Personenkategorie anlegen"
date: 2019-04-27T02:35:11+02:00
weight: 14
pre: "<b>4.</b> "
---

Gehe auf die Übersicht Einstellungen -> Personenkategorien:

![](/images/openolitor/Aufgabe4_1.jpg)

Klicke auf „+ Personenkategorie erstellen“

![](/images/openolitor/Aufgabe4_2.jpg)

![](/images/openolitor/Aufgabe4_3.jpg)

Fülle wie folgt aus:

- Personenkategorie: AG- digiTrans
- Beschreibung: Arbeitsgruppe für digitale Transformation

![](/images/openolitor/Aufgabe4_4.jpg)

Klicke auf Speichern:

![](/images/openolitor/Aufgabe4_5.jpg)
