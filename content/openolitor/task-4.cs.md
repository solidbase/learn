---
title: "Přidat kategorie osob"
date: 2019-04-27T02:35:11+02:00
weight: 14
pre: "<b>4.</b> "
---

V menu běžte do Nastavení -> Kategorie osob 

![](/images/openolitor/task4_cz_1.jpg)

Klikněte na "Vytvořit kategorii osob"

![](/images/openolitor/task4_cz_2.jpg)

![](/images/openolitor/task4_cz_3.jpg)

Vyplňte detaily:

- Kategorie osob: 
- Popis:

![](/images/openolitor/task4_cz_4.jpg)

Klikněte na uložit

![](/images/openolitor/task4_cz_5.jpg)
