---
title: "Monatliche Abrechnung erstellen und SEPA XML generieren"
date: 2019-04-27T02:35:11+02:00
weight: 16
pre: "<b>6.</b> "
---

Gehe zur Übersicht Mitglied -> Mitglieder

![](/images/openolitor/Aufgabe6_1.jpg)

Filtere nach Typ und wähle alle „MonatszahlerInnen“ aus.

![](/images/openolitor/Aufgabe6_2.jpg)

Wähle alle MonatszahlerInnen aus und klicke auf „Aboliste anzeigen“

![](/images/openolitor/Aufgabe6_3.jpg)

Damit wechselst du in die Übersicht der Anteile der Mitglieder, die eine Monatliche Bezahlung ausgewählt haben.

![](/images/openolitor/Aufgabe6_4.jpg)

Für jedes dieser Anteile können wir nun eine monatliche Rechnung erstellen, die entweder verschickt werden kann, oder einfach als offener Posten gehandhabt werden kann.

Auf der Übersicht der Anteile, filtere zunächst nach Aktiven Anteilen. Wähle dann alle aus und klicke auf „Rechnungspositionen erstellen“:

![](/images/openolitor/Aufgabe6_5.jpg)

Auf der rechten Seite des Bildschirms öffnet sich ein Fenster:

![](/images/openolitor/Aufgabe6_6.jpg)

Fülle wie folgt aus:

- Titel: zum Beispeil „Rechnung Juni“
- Anzahl der Lieferungen: 1

{{% notice note %}}
Hier kommt das Konzept Guthaben/Preis pro Lieferung noch einmal heraus. Da wir beim Anteilstyp den Monatsbeitrag als „Preis pro Lieferung“ definiert haben, bedeutet also eine Rechnung für einen Monat, eine Rechnung für eine Lieferung.
{{% /notice %}}

Klicke auf „Erstellen“

![](/images/openolitor/Aufgabe6_7.jpg)

Die Rechnungspositionen wurden erstellt und es erscheint ein Button mit der Möglichkeit sich direkt die gerade erstellen Rechnungspositionen anzuzeigen:

![](/images/openolitor/Aufgabe6_8.jpg)

Klicke dazu auf „… Rechnungspositionen anzeigen“.

Wir wählen nun alle, gerade erzeugten Rechnungspositionen aus und generieren damit Rechnungen. Es ist möglich dies auch über die Ansicht Finanzen -> Rechnungspositionen durchzuführen. Der Unterschied zu Rechnungsposition und Rechnung ist, das mehrerer Rechnungsposition pro Mitglied in eine Rechnung zusammengefasst werden können. Zum Beispiel, wenn ein Mitglied mehrere Anteile bezieht.

![](/images/openolitor/Aufgabe6_9.jpg)

Es erscheint das folgende Fenster auf der rechten Bildschirmseite:

![](/images/openolitor/Aufgabe6_10.jpg)

Wähle nun einen Titel, ein Rechnungsdatum und ein Fälligkeitsdatum aus und klicke „Erstellen“.

![](/images/openolitor/Aufgabe6_11.jpg)

Auch hier gibt es mit dem Button „Rechnungen anzeigen“ die Möglichkeit, direkt die gerade erstellten Rechnungen anzusehen:

![](/images/openolitor/Aufgabe6_12.jpg)

Diese können wir nun nach Zahlmethode (Payment Type) filtern. In unserem Fall nach Lastschrift (DirectDebit), dann die gefilterten Rechnungen auswählen und im Menüpunkt „pain.008.001.07 erstellen“ klicken.

![](/images/openolitor/Aufgabe6_13.jpg)

{{% notice note %}}
Die Bezeichnungen (Payment type, Direct Debit, pain…) sind temporär und werden noch in Deutsch übersetzt. Pain.008.001.07 beschreibt ein XML Dokument, das von der Bank angenommen wird um Lastschriften einzuziehen. Dieses XML Dokument wird mit OpenOlitor erstellt und kann über ein online Portal an die Bank übermittelt werden.
{{% /notice %}}