---
title: "Arbeitsangebot erstellen"
date: 2019-04-27T02:35:11+02:00
weight: 18
pre: "<b>8.</b> "
---

Gehe zur Übersicht Mitarbeit -> Arbeitsangebote

![](/images/openolitor/Aufgabe8_1.jpg)

Klicke auf „+Arbeitsangebot erstellen“. Die folgende Detailansicht öffnet sich:

![](/images/openolitor/Aufgabe8_2.jpg)

Fülle wie folgt aus:

- Titel: Unkraut jäten
- Ort: Witzenhausen
- Start: 01.06.2019, 8:00
- Ende: 01.06.2019, 12:00

Klicke auf „Arbeitsangebot erstellen“.

Dabei erscheint unter „Aktionen“ die Möglichkeit das Arbeitsangebot zu multiplizieren.

![](/images/openolitor/Aufgabe8_3.jpg)

Drücke auf Multiplizieren und das folgende Fenster öffnet sich:

![](/images/openolitor/Aufgabe8_4.jpg)

Wähle den wöchentlichen Rhythmus bis zum 01.07.2019 aus und klicke auf „Arbeitsangebot erstellen“.

Gehe zurück zur Übersicht der Arbeitsangebote:

![](/images/openolitor/Aufgabe8_5.jpg)

Die Angebote sind erst dann im Mitgliederportal zu sehen, wenn sie freigeschaltet wurden. Klicke dazu auf ein Arbeitsangebot hinein und wähle „Freischalten“.

![](/images/openolitor/Aufgabe8_6.jpg)
