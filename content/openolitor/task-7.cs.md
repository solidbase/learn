---
title: "Napsat email všem podílníkům na jednom výdejním místě"
date: 2019-04-27T02:35:11+02:00
weight: 17
pre: "<b>7.</b> "
---

Přejděte na nastavení -> Výdejní místa

![](/images/openolitor/task7_cz_1.jpg)

Zvolte jedno výdjení místo zaškrtnutím okénka. Zvolte si z menu co chcete udělat "Odeslání emailu zákazníkům" 

![](/images/openolitor/task7_cz_2.jpg)

Otevře se okno externího mailvého klienta se všemi adresami podílníků na daném výdejím místě. 

{{% notice note %}}
Existuje možnost poslat emaily také přímo z systému Open Olitor spíše než z externího mailového klienta, tuto možnost najdete pod tlačítkem E-mailový formulář. 
{{% /notice %}}