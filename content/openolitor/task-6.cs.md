---
title: "Vytovřit měsíční doklady"
date: 2019-04-27T02:35:11+02:00
weight: 16
pre: "<b>6.</b> "
---

Přejděte do položky Podílník -> Podílníci

![](/images/openolitor/task6_cz_1.jpg)

Vyfiltrujte podle typu a zvolte všechny s "měsíční platbou" .

![](/images/openolitor/task6_cz_2.jpg)

Zvolte všechny vybrané členy kliknutím na zaškrtávací okénko a kliknutím na "Zobrazit seznam odběrů"

![](/images/openolitor/task6_cz_3.jpg)

To umožní získat filtrované zobrazení odběrů. Které ukáže pouze podílníky, kteří platí měsíčně. his enables you to get a filtered view of the subscriptions. Showing only those subscriptions of members who pay monthly. 

![](/images/openolitor/task6_cz_4.jpg)

Pro každý z těchto odběrů můžeme vygenerovat měsíční doklady. Dokumenty mohou být poslány tištěné či mailem nebo použity jako otevřená položka pro sledování plateb. 

Z vyfiltrovaného zobrazení zvolte podíly, které jsou akttivní. Klikněte na "Vytvořit položky dokladu" 

![](/images/openolitor/task6_cz_5.jpg)

Na pravé straně obrazovky se otevřené nové okno:

![](/images/openolitor/task6_cz_6.jpg)

Zvolte podrobnosti:

- Název: zvolte název např. doklady za červen"
- Počet závozů: 1

{{% notice note %}}
Zde se dostáváme k tématu bilací / cena za dodávku. Vzpomeňte si, když jsme určovali typ podílu měli jsme monžost stanovit cenu za dodávku. Pokud však vybíráme peníze měsíčně, bez ohledu na počet dodávek, stanovíme stejnou cenu a chováme se k "ceně za dodávku" a k "ceně za měsíc" jako k rovnocenným položkám. Nyní naše doklady znamenají, že v dokladu se 1 dodávka rovná dokladu za 1 měsíc. 

{{% /notice %}}

Klikěnte "Vytvořit"

![](/images/openolitor/task6_cz_7.jpg)

Doklady byly vytvořeny a objeví se tlačítko s možností zborazit pouze vytvořené doklady:

![](/images/openolitor/task6_cz_8.jpg)

Klikněte " X zobrait položky dokladu".

Nyní vybereme všechny právě vytvořené položky dokladu a vytvoříme doklady. Je také možné přejít do menu Finance -> Položky dokladu a zvolit položky dokladu, které chceme spojit do odkladů. Rozdíl mezi položkou dokladu a dokladem je, že různé položky dokladu, které patří témuž podílníkovi se sjendotí do jednoho dokladu. To se stane v případě pokud vystavujeme doklad za více měsíců nebo podílník má více druhů podílu. 

![](/images/openolitor/task6_cz_9.jpg)

Napravo se objeví toto okno: 

![](/images/openolitor/task6_cz_10.jpg)

Nyní zvolte název, datum a splatnost a klikněte na "Vytvořit"

![](/images/openolitor/task6_cz_11.jpg)

Opět se objeví tlačítko, kterým se dostanete rovnou k vytvořeným dokladům: 

![](/images/openolitor/task6_cz_12.jpg)

