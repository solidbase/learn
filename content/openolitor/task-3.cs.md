---
title: "Přidat nový produkt"
date: 2019-04-27T02:35:11+02:00
weight: 13
pre: "<b>3.</b> "
---

V menu jděte na Nastavení -> Produkty:

![](/images/openolitor/task3_cz_1.png)

Klikněte na "vytvořit produkt"

![](/images/openolitor/task3_cz_2.png)

![](/images/openolitor/task3_cz_3.png)

Vyplňte řádky následujícími údaji:

- Označení:
- Obvyklé množství: XY
- Jednotka:
- Kategorie :
- Dodavatel:


Klikněte na uložit.

![](/images/openolitor/task3_en_5.jpg)
