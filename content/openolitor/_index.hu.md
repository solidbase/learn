---
title: "OpenOlitor"
date: 2019-04-27T02:15:59+02:00
---


{{% notice tip %}}
[General Handling](./general-handling)
{{% /notice %}}


Sample exercises:

1. [Add new member](./task-1)
2. [Add new Subscription type](./task-2)
3. [Add new product](./task-3)
4. [Add new person category](./task-4)
5. [Creat new delivery planning](./task-5)
6. [Create monthly invoice and generate SEPA XML](./task-6)
7. [Write e-mail to all members of one depot](./task-7)
8. [Create a new work opportunity](./task-8)
