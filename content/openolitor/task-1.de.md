---
title: "Neues Mitglied hinzufügen"
date: 2019-04-27T02:35:11+02:00
weight: 11
pre: "<b>1.</b> "
---

Gehe auf die Übersicht aller Mitglieder unter dem Menüpunkt Mitglied -> Mitglieder

![](/images/openolitor/Aufgabe1_1.jpg)

Drücke auf den Button „Mitglied erstellen“

![](/images/openolitor/Aufgabe1_2.jpg)

Eine Detailansicht öffnet sich.
Alle rot umrandeten Felder sind Pflichtfelder. Der Button „Mitglied erstellen“ ist so lange inactive, bis alle Pflichtfelder ausgefüllt sind.

{{% notice tip %}}
Details zu Ansprechperson und Mitgliedsdetails (Adresse) zuerst ausfüllen. Dann auf „Mitglied erstellen“ drücken. Beim Speichern werden die bereits eingegebenen Information (Adresse & Name)  in den Abschnitt „Zahlungsdetails“ übernommen.
{{% /notice %}}

![](/images/openolitor/Aufgabe1_3.jpg)

Nach dem Speichern erscheinen Neue Felder in der Mitglieder-Detailübersicht. Diese sind: „Aufgaben“, „Anteile“, „Rechnungen“. Unter dem Punkt „Anteile“ werden wir jetzt dem Mitglied ein Ernteanteil zuweisen.

{{% notice note %}}
Mitglieder können auch aus mehreren AnsprechpartnerInnen bestehen. Zum Beispiel Familien, oder WGs, die sich ein Anteil teilen, aber als gemeinsames Mitglied agieren.
{{% /notice %}}

![](/images/openolitor/Aufgabe1_4.jpg)

Um ein Ernteanteil dem Mitglied hinzuzufügen, drücke auf den Button „+Neuer Anteil“

![](/images/openolitor/Aufgabe1_5.jpg)

Auf der Rechten Seite des Bildschirms erscheint ein Auswahlprozess. Zunächst wird der Anteilstyp gewahlt, dann der Vertrieb (z.B. der Tag) , dann die Vertriebsart (z.B. die Abholstelle) und zum Schluss das Startdatum.

{{% notice note %}}
Wenn an dem Tag des Startdatums bereits eine Lieferung stattfinden soll, bitte eine Tag vorher als Startdatum angeben.
{{% /notice %}}

Klicke auf „Anteil erstellen“ und das Anteil erscheint in der Übersicht des Mitglieds:

![](/images/openolitor/Aufgabe1_6.jpg)

Ein Klick auf die Anteilsnummer (oben in Gelb markiert), öffnet die Detailansicht des jeweiligen Anteils und erlaubt Änderungen:  Löschen, Guthaben anpassen, Vertriebsart anpassen, Manuelle Rechnung erstellen, Richtpreis anpassen, Zusatzsanteile hinzufügen, Abwesenheiten erfassen.

![](/images/openolitor/Aufgabe1_7.jpg)
