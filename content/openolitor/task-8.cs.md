---
title: "Vytvořit nabídku práce"
date: 2019-04-27T02:35:11+02:00
weight: 18
pre: "<b>8.</b> "
---

Přejděte na Spolupráce -> Nabídka práce

![](/images/openolitor/task8_cz_1.jpg)

Klikněte na "Vytvořit nabídku práce" objeví se následující detaily 

![](/images/openolitor/task8_cz_2.jpg)

Vyplňte požadované údaje.

Klikněte na "Vytvořit nabídku práce".

Objeví se nové tlačítko na řádku "Činnosti", kterým je možné znásobit akci, pokud je nabídka práce opakující. 
![](/images/openolitor/task8_cz_3.jpg)

Klikněte na vynásobit a otevře se následující:

![](/images/openolitor/task8_cz_4.jpg)

Zvolte týdenní četnost a koncové datum a klikněte na "Vytvořit nabídku práce".

Vraťe se do přehledu nabíde:

![](/images/openolitor/task8_cz_5.jpg)

Nabídka se na členském portále zobrazí teprve tehdy až kliknete na "Publikovat".

![](/images/openolitor/task8_cz_6.jpg)
