---
title: "Neues Produkt anlegen"
date: 2019-04-27T02:35:11+02:00
weight: 13
pre: "<b>3.</b> "
---

Gehe auf die Übersicht Einstellungen -> Produkte:

![](/images/openolitor/Aufgabe3_1.jpg)

Klicke auf „+Produkt erstellen“

![](/images/openolitor/Aufgabe3_2.jpg)

![](/images/openolitor/Aufgabe3_3.jpg)

Fülle die Zeile wie folgt aus:

- Bezeichnung: Physalis
- Menge: 6
- Einheit: St. 
- Kategorien: Gemüse
- Produzenten: WW

![](/images/openolitor/Aufgabe3_4.jpg)

Klicke auf Speichern:

![](/images/openolitor/Aufgabe3_5.jpg)
