---
title: "Add new product"
date: 2019-04-27T02:35:11+02:00
weight: 13
pre: "<b>3.</b> "
---

Go to the menue item Settings -> Products: 

![](/images/openolitor/task3_hu_1.jpg)

Click "create a product"

![](/images/openolitor/task3_hu_2.jpg)

![](/images/openolitor/task3_hu_3.jpg)

Fill in the row with the following information: 

-Designation: Physalis
-A

- Bezeichnung: Physalis
- Default quantity: 6
- Unit. piece
- Category: Gemüse
- Producer: WW

![](/images/openolitor/task3_hu_4.jpg)

Click on the Save button

![](/images/openolitor/task3_hu_5.jpg)
