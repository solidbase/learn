---
title: "Email an alle Mitglieder einer Abholstelle schreiben"
date: 2019-04-27T02:35:11+02:00
weight: 17
pre: "<b>7.</b> "
---

Go to the menu item Settings -> Depots
Gehe zu Einstellungen -> Abholstationen

![](/images/openolitor/task7_en_1.jpg)

Choose one of the depots by ticking the checkbox. Choose from the menue the action "Send email to members"

![](/images/openolitor/task7_en_2.jpg)

A new window of your external mail client opens including all e-mail addresses of members that pick up from the selected depot. 

{{% notice note %}}
There is also the option to send e-mails directly from the OpenOlitor system rather than using an external mail client (e.g. Thunderbird, Outlook, etc.). You can find this option under the menue item "E-mail formular". 
{{% /notice %}}