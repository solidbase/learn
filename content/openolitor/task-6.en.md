---
title: "Generate monthly invoice including SEPA XML export file"
date: 2019-04-27T02:35:11+02:00
weight: 16
pre: "<b>6.</b> "
---

Go to menu item Member -> Members

![](/images/openolitor/task6_en_1.jpg)

Filter by type and choose all that have "monthly payment" .

![](/images/openolitor/task6_en_2.jpg)

Select all filtered members by clicking the checkbox and click "Show subscriptions"

![](/images/openolitor/task6_en_3.jpg)

This enables you to get a filtered view of the subscriptions. Showing only those subscriptions of members who pay monthly. 

![](/images/openolitor/task6_en_4.jpg)

For each of these subscriptions we can generate a monthly invoice. The document can be send (pyhsically or via email) or just used as an open item to track payments.

From the filtered view, choose subscriptions that are active. Now click "Create invoice items". 

![](/images/openolitor/task6_en_5.jpg)

A new windows opens on the right side of the display: 

![](/images/openolitor/task6_en_6.jpg)

Fill in the details as follows:

- Title: Choose a title for example "invoice July"
- Number of deliveries: 1

{{% notice note %}}
Here we stumble upon the concept of balance/price per delivery. You may recall that when defining the subscription type we only had the option to set a price per delivery. If we collect money monthly, independent of the number of deliveries, we set the same price and treated "price per delivery" and "price per month" equivalent. Now for our invoices this means an invoice for 1 delivery equals an invoice for 1 month. 

{{% /notice %}}

Click "Create"

![](/images/openolitor/task6_en_7.jpg)

The invoice items were created and a button appears with the option to view the just created invoice items: 

![](/images/openolitor/task6_en_8.jpg)

Click " X show invocie items".

We now select all just created invoice items and create invoices. It is also possible to go to the menu item Finances -> Invoice itmes and manually select the invoice items that we want to combine to invoices. The difference between invoice item and invoice is that multiple invoice items that belong to the same member will be combined to one invoice. This could be if we want to invoice several months, or if one member has more than one subscription. 

![](/images/openolitor/task6_en_9.jpg)

The following window opens on the right side of the display: 

![](/images/openolitor/task6_en_10.jpg)

Now choose a title, an invoice date and a date due and click "Create"

![](/images/openolitor/task6_en_11.jpg)

Again a button appears to go directly to the just created invoices: 

![](/images/openolitor/task6_en_12.jpg)

We now can filter the new invoices by payment type. In our case we want to filter all that pay via SEPA direct debit. Therefore, we select Direct debit. Select the filtered invoices by ticking the checkbox and choose in the menu the action "Create SEPA export file"

![](/images/openolitor/task6_en_13.jpg)

{{% notice note %}}
The generated XML file can be send to your bank to process the direct debit payments. 
{{% /notice %}}